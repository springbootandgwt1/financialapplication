package com.nss.financial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nss.financial.pojo.FinancialExpensePojo;

@Repository
public interface FinancialExpenseRepository extends JpaRepository<FinancialExpensePojo, Long> {

}
