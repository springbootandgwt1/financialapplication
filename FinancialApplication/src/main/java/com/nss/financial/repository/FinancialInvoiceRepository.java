package com.nss.financial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nss.financial.pojo.FinancialInvoicePojo;

@Repository
public interface FinancialInvoiceRepository extends JpaRepository<FinancialInvoicePojo, Long>{

}
