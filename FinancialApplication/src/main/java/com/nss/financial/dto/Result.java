package com.nss.financial.dto;

import lombok.Data;

@Data
public class Result {
	
	private int statusCode;
	private String status;
	private Object responseObject;

}
