package com.nss.financial.dto;

import lombok.Data;

@Data
public class FinancialProfitAndLossResponse {
	
	private Double invoiceAmount;
	private String productName;
	private Double expenseAmount;
	private String profitOrLossAmount;
	private String profitOrLossPercentage;
	private String status;
	
	public static final String decimalFormat = "%.2f";
	
	
	public String getProfitOrLossAmount() {
		if(invoiceAmount !=null && expenseAmount != null) {
			if(invoiceAmount>expenseAmount) {
				Double amount = (invoiceAmount-expenseAmount);
				return getFormat(amount);
			}else {
				Double amount = (expenseAmount-invoiceAmount);
				return getFormat(amount);
			}
		}else {
			return "0.00";
		}
	}
	
	public String getProfitOrLossPercentage() {
		try {
			if(invoiceAmount !=null && expenseAmount != null) {
				if(invoiceAmount>expenseAmount) {
					Double amount = (invoiceAmount-expenseAmount)/(expenseAmount) *100;
					return getFormat(amount)+" %";
				}else {
					Double amount = (expenseAmount-invoiceAmount)/(expenseAmount) *100;
					return getFormat(amount)+" %";
				}
				
			}else {
				return "0.00 %";
			}
		}catch(Exception ex){
			return "0.00 %";
		}
	}
	
	public String getStatus() {
		if(invoiceAmount !=null && expenseAmount != null) {
			if(invoiceAmount>expenseAmount) {
				return "Profit";
			}else {
				return "Loss";
			}
		}else {
			return "N/A";
		}
	}
	
	public static String getFormat(Double amount) {
		return String.format(decimalFormat, amount);
	}
	
}
