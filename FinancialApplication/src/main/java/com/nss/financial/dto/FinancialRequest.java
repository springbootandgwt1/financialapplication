package com.nss.financial.dto;

import lombok.Data;

@Data
public class FinancialRequest {
	
	// For Inovoice Form
	private String invoiceNumber;
	private Double invoiceAmount;
	private String productName;
	private String remarks;
	
	// For Expense Form
	private String receiptNumber;
	private Double expenseAmount;

}
