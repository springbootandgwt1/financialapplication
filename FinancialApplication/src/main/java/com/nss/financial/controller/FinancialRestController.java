package com.nss.financial.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nss.financial.dto.FinancialRequest;
import com.nss.financial.dto.Result;
import com.nss.financial.service.FinancialService;

@RestController
@RequestMapping("financial")
public class FinancialRestController {
	
	@Autowired
	private FinancialService financialServiceImpl;
	
	
	@PostMapping("saveInvoiceDetials")
	public Result saveInvoiceDetials(@RequestBody FinancialRequest finRequest) {
		Result result = new Result();
		financialServiceImpl.saveInvoiceDetials(finRequest);
		result.setStatusCode(HttpStatus.OK.value());
		result.setStatus(HttpStatus.OK.name());
		return result;
	}
	
	@GetMapping("getAllInvoiceDetails")
	public Result getAllInvoiceDetails() {
		Result result = financialServiceImpl.getAllInvoiceDetails();
		result.setStatusCode(HttpStatus.OK.value());
		result.setStatus(HttpStatus.OK.name());
		return result;
	}
	
	@PostMapping("saveExpenseDetails")
	public Result saveExpenseDetails(@RequestBody FinancialRequest finRequest) {
		Result result = new Result();
		financialServiceImpl.saveExpenseDetails(finRequest);
		result.setStatusCode(HttpStatus.OK.value());
		result.setStatus(HttpStatus.OK.name());
		return result;
	}
	
	@GetMapping("getAllExpenseDetails")
	public Result getAllExpenseDetails() {
		Result result = financialServiceImpl.getAllExpenseDetails();
		result.setStatusCode(HttpStatus.OK.value());
		result.setStatus(HttpStatus.OK.name());
		return result;
	}
	
	@GetMapping("getProfitAndLossReport")
	public Result getProfitAndLossReport() {
		Result result = financialServiceImpl.getProfitAndLossReport();
		result.setStatusCode(HttpStatus.OK.value());
		result.setStatus(HttpStatus.OK.name());
		return result;
	}
	

}
