package com.nss.financial.pojo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="EXPENSE")
@Data
public class FinancialExpensePojo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long expenseId;
	
	@Column(name="RECEIPT_NUMBER")
	private String receiptNumber;
	
	@Column(name="EXPENSE_AMOUNT")
	private Double expenseAmount;
	
	@Column(name="RECEIPT_DATE")
	private Timestamp receiptDate;
	
	@Column(name="PRODUCT_NAME")
	private String productName;
	
	@Column(name="REMARKS")
	private String remarks;

}
