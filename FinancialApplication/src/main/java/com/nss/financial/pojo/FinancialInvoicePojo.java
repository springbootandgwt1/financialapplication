package com.nss.financial.pojo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="INVOICE")
@Data
public class FinancialInvoicePojo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long invoiceId;
	
	@Column(name="INVOICE_NUMBER")
	private String invoiceNumber;
	
	@Column(name="INVOICE_AMOUNT")
	private Double invoiceAmount;
	
	@Column(name="INVOICE_DATE")
	private Timestamp invoiceDate;
	
	@Column(name="PRODUCT_NAME")
	private String productName;
	
	@Column(name="REMARKS")
	private String remarks;

}
