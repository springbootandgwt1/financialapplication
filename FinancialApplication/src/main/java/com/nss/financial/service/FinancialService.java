package com.nss.financial.service;

import com.nss.financial.dto.FinancialRequest;
import com.nss.financial.dto.Result;

public interface FinancialService {
	
	public void saveInvoiceDetials(FinancialRequest finRequest);

	public Result getAllInvoiceDetails();

	public void saveExpenseDetails(FinancialRequest finRequest);

	public Result getAllExpenseDetails();

	public Result getProfitAndLossReport();

}
