package com.nss.financial.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nss.financial.dto.FinancialProfitAndLossResponse;
import com.nss.financial.dto.FinancialRequest;
import com.nss.financial.dto.Result;
import com.nss.financial.pojo.FinancialExpensePojo;
import com.nss.financial.pojo.FinancialInvoicePojo;
import com.nss.financial.repository.FinancialExpenseRepository;
import com.nss.financial.repository.FinancialInvoiceRepository;
import com.nss.financial.service.FinancialService;

@Service
public class FinancialServiceImpl implements FinancialService {
	
	@Autowired
	private FinancialInvoiceRepository financialInvoiceRepository;
	
	@Autowired 
	private FinancialExpenseRepository financialExpenseRepository;
	
	@Override
	public void saveInvoiceDetials(FinancialRequest finRequest) {
		FinancialInvoicePojo finInvoicePojo = new FinancialInvoicePojo();
		finInvoicePojo.setInvoiceAmount(finRequest.getInvoiceAmount());
		finInvoicePojo.setInvoiceDate(new Timestamp(new Date().getTime()));
		finInvoicePojo.setInvoiceNumber(finRequest.getInvoiceNumber());
		finInvoicePojo.setProductName(finRequest.getProductName());
		finInvoicePojo.setRemarks(finRequest.getRemarks());
		financialInvoiceRepository.save(finInvoicePojo);
	}

	@Override
	public Result getAllInvoiceDetails() {
		Result result = new Result();
		List<FinancialInvoicePojo> finInvoicePojos = financialInvoiceRepository.findAll();
		
		result.setResponseObject(finInvoicePojos);
		return result;
	}

	@Override
	public void saveExpenseDetails(FinancialRequest finRequest) {
		FinancialExpensePojo finExpensePojo = new FinancialExpensePojo();
		finExpensePojo.setExpenseAmount(finRequest.getExpenseAmount());
		finExpensePojo.setReceiptDate(new Timestamp(new Date().getTime()));
		finExpensePojo.setReceiptNumber(finRequest.getReceiptNumber());
		finExpensePojo.setProductName(finRequest.getProductName());
		finExpensePojo.setRemarks(finRequest.getRemarks());
		financialExpenseRepository.save(finExpensePojo);
	}

	@Override
	public Result getAllExpenseDetails() {
		Result result = new Result();
		List<FinancialExpensePojo> finExpensePojos = financialExpenseRepository.findAll();
		
		result.setResponseObject(finExpensePojos);
		return result;
	}

	@Override
	public Result getProfitAndLossReport() {
		Result result = new Result();
		Map<String, FinancialProfitAndLossResponse> finMap = new LinkedHashMap<>();
		List<FinancialProfitAndLossResponse> finProfitOrLossRespList = new ArrayList<>();
		List<FinancialExpensePojo> finExpensePojos = financialExpenseRepository.findAll();
		List<FinancialInvoicePojo> finInvoicePojos = financialInvoiceRepository.findAll();
		if(!finExpensePojos.isEmpty() && !finInvoicePojos.isEmpty()) {
			for(FinancialExpensePojo finExpensePojo : finExpensePojos) {
				if(finExpensePojo.getProductName() !=null && finExpensePojo.getExpenseAmount() !=null) {
					FinancialProfitAndLossResponse finProfitOrLoss = new FinancialProfitAndLossResponse();
					if(finMap.containsKey(finExpensePojo.getProductName())) {
						finProfitOrLoss = finMap.get(finExpensePojo.getProductName());
					}
					
					if(finProfitOrLoss.getExpenseAmount() ==null) {
						finProfitOrLoss.setExpenseAmount(0.0);
					}
					finProfitOrLoss.setProductName(finExpensePojo.getProductName());
					finProfitOrLoss.setExpenseAmount(finProfitOrLoss.getExpenseAmount()+finExpensePojo.getExpenseAmount());
					finMap.put(finExpensePojo.getProductName(), finProfitOrLoss);
				}
			}
			
			for(FinancialInvoicePojo finInvoicePojo : finInvoicePojos) {
				if(finInvoicePojo.getProductName() !=null && finInvoicePojo.getInvoiceAmount() !=null) {
					FinancialProfitAndLossResponse finProfitOrLoss = new FinancialProfitAndLossResponse();
					if(finMap.containsKey(finInvoicePojo.getProductName())) {
						finProfitOrLoss = finMap.get(finInvoicePojo.getProductName());
					}
					
					if(finProfitOrLoss.getInvoiceAmount() ==null) {
						finProfitOrLoss.setInvoiceAmount(0.0);
					}
					finProfitOrLoss.setProductName(finInvoicePojo.getProductName());
					finProfitOrLoss.setInvoiceAmount(finProfitOrLoss.getInvoiceAmount()+finInvoicePojo.getInvoiceAmount());
					finMap.put(finInvoicePojo.getProductName(), finProfitOrLoss);
				}
			}
			
			for(Map.Entry<String, FinancialProfitAndLossResponse> entry : finMap.entrySet()) {
				finProfitOrLossRespList.add(entry.getValue());
			}
		}
		result.setResponseObject(finProfitOrLossRespList);
		return result;
	}

}
